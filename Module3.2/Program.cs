﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
           

            Console.WriteLine("Hello world!");
            Console.ReadKey();
        }
    }

    public class Task4
    {
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && result >= 0)
                return true;
            else
            {
                Console.WriteLine("Enter natural number");
                input = Console.ReadLine();
                return TryParseNaturalNumber(input, out result);
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] fibo = new int[n];
            if (n != 0)
            { 
            if (n == 1){ fibo[0] = 0;}
            else 
                {
                fibo[0] = 0;
                fibo[1] = 1;
                for (int i = 2; i < n; i++)
                    {
                    fibo[i] = fibo[i - 2] + fibo[i - 1];
                    }
                }
            }
             return fibo;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int abs = Math.Abs(sourceNumber);
            char[] temp = abs.ToString().ToCharArray();
            Array.Reverse(temp);
            if (sourceNumber > 0)
            {
                return int.Parse(temp);
            }
            else
            {
                return (int.Parse(temp)*(-1));
            }
            
        }
    }

    public class Task6
    {
         public int[] GenerateArray(int size)
        {
            try
            {
                return new int[size];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new int[0];
            }
   
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == 0)
                    source[i] = 0;
                else 
                    {
                    source[i] = -source[i];
                    }
            }
            return source;
        }
    }

    public class Task7
    {
         public int[] GenerateArray(int size)
        {
            try
            {
                return new int[size];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new int[0];
            }
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> vs = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                    vs.Add(source[i]);
            }
            return vs;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int start = 1;
            int[,] result = new int[size, size];

            int i = 0, j = 0;
            int count;
            while (size != 0)
            {
                    for (count = 0; count < size - 1; count++)
                    {
                        result[i, j++] = start;
                        start++;
                    }
                    for (count = 0; count < size - 1; count++)
                    {
                        result[i++, j] = start;
                        start++;
                    }
                    for (count = 0; count < size-1; count++)
                    {
                        result[i, j--] = start;
                        start++;
                    }
                    for (count = 0; count < size-1; count++)
                    {
                        result[i--, j] = start;
                        start++;
                    }
                i++;j++;
                if (size < 2) size = 0;
               else size = size - 2;
                if (result[i, j] == 0) result[i, j] = start;
               
            }

            return result;

            
        }
    }
}
